function saveAll(objects) {
  var obj = objects.pop();

  obj.save(function(err, saved){
    if (err) {
      return console.error(err);
    }

    if (objects.length > 0) {
      saveAll(objects);
    }
  });
}

module.exports.saveAll = saveAll