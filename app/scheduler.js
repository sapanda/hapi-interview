var schedule = require('node-schedule');
var runUpdateTask = require('./tasks.js');

// Schedule tasks every 30 minutes
var rule = new schedule.RecurrenceRule();
rule.minute = new schedule.Range(0, 59, 30);

module.exports = function scheduleTasks() {
    var job = schedule.scheduleJob(rule, function() {
        runUpdateTask();
    });
}