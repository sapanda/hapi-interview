var https = require('https');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/hapidb');

var models = require('./models.js')(mongoose);
var API_URL = 'https://api.humanapi.co/v1/human/';

// Load the list of (humanId, lastUpdateTime, endpoint) tuples from the DB
function loadData(callback) {
  models.LastUpdateTime.find(callback);
}

// Iterate through each document in the dataset and make a query to the Data API
function fetchUpdates(dataset, callback) {
  if (dataset.length > 0) {
    var data = dataset.pop();

    var accessToken = data['accessToken'];
    var lastUpdateTime = data['lastUpdateTime'];

    // TODO: Actually get model using data['endpoint']
    //       For now, assume its a SleepSummary
    var model = models.Endpoints.SleepSummary;

    requestUpdate(model, accessToken, lastUpdateTime, function(err, data) {
      callback(err, data, model);
      fetchUpdates(dataset, callback);
    });
  }
}

// Make the actual query to the Data API given the accessTokena and lastUpdateTime
function requestUpdate(model, accessToken, lastUpdateTime, callback) {
  var url = API_URL + model.endpoint +
    '?access_token=' + accessToken +
    '&updated_since=' + lastUpdateTime;

  console.log('Making request to: ' + url);

  return https.get(url, function(res) {

    var data = '';

    res.on('data', function(chunk){
      data += chunk;
    });

    res.on('end', function(){
      decodedData = data.toString('utf8');
      jsonData = JSON.parse(decodedData);
      callback(null, jsonData);
    });

  }).on('error', function(err) {
    callback(err);
  });
}

// Queue to store a list of list of records that need to be updated
var updateQueue = [];

// Upsert all data objects recursively
function upsertAll(objects, model, lastUpdateTime) {
  var object = objects.pop();

  var updateTime = Date.parse(object['updatedAt']);
  if (lastUpdateTime) {
    lastUpdateTime = Math.max(lastUpdateTime, updateTime);
  } else {
    lastUpdateTime = updateTime;
  }

  var query = {
    humanId: object['humanId'],
    date: object['date'],
    source: object['source']
  }

  model.update(query, object, {upsert: true}, function(err) {
    if (err) {
      return console.error(err);
    }

    if (objects.length > 0) {
      upsertAll(objects, model, lastUpdateTime);
    } else {
      upsertLastUpdateTime(query.humanId, lastUpdateTime, function(err) {
        if (err) console.error(err);
        updateQueue.pop();
        upsertNext();
      });
    }
  });
}

// Update the LastUpdateTime
function upsertLastUpdateTime(humanId, lastUpdateTime, callback) {
  var query = {humanId: humanId};
  var update = {lastUpdateTime: new Date(lastUpdateTime).toISOString()};

  models.LastUpdateTime.update(query, update, function(err) {
    callback(err);
  });
}

function upsertNext() {
  // If only item in the queue, then begin saving
  if (updateQueue.length == 1) {
    upsertAll(updateQueue[0].objects, updateQueue[0].model);
  }
}

// Update the Database with the objects obtained from the Data API response
function updateDB(objects, model) {
  updateQueue.push({
    objects: objects,
    model: model
  });

  upsertNext();
}

// Run the task that goes through the database, requests the latest data, and updates the database
module.exports = function runUpdateTask() {

  var saveCallback = function(err, data, model) {
    if (err) {
      console.error(err);
    } else {
      normalizedData = model.normalize(data);
      updateDB(normalizedData, model.model);
    }
  }

  var fetchCallback = function(err, data) {
    if (err) {
      console.error(err);
    } else if (data) {
      fetchUpdates(data, saveCallback);
    }
  }

  loadData(fetchCallback);
}