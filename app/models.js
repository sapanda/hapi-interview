var dbutils = require('./dbutils.js');

function generateSchemas(mongoose) {
  var schemas = new Object();

  // TODO: Add other endpoint schemas here
  schemas.SleepSummary = new mongoose.Schema({
    humanId: {
      type: String,
      index: true
    },
    id: String,
    userId: String,
    date: {
      type: String,
      index: true
    },
    source: {
      type: String,
      index: true
    },
    timeAsleep: Number,
    timeAwake: Number,
    createdAt: String,
    updatedAt: String
  });

  schemas.LastUpdateTime = new mongoose.Schema({
    humanId: {
      type: String,
      index: true
    },
    accessToken: {
      type: String,
      index: true
    },
    lastUpdateTime: String,
    endpoint: String
  });

  return schemas;
}

function generateModels(mongoose, schemas) {
  var models = new Object();

  models.Endpoints = {
    // TODO: Add models for other endpoints here
    SleepSummary : {
      model : mongoose.model('SleepSummary', schemas.SleepSummary),
      endpoint : 'sleeps/summaries',
      collection: 'sleepsummaries',
      normalize : function(data) {
        // TODO: Actually normalize the data
        // TODO: No callbacks since it assumes normalization is computationally efficient
        // TODO: Need to pass back error
        return data;
      }
    }
  };

  models.LastUpdateTime = mongoose.model('LastUpdateTime', schemas.LastUpdateTime);

  clearModels(models);

  return models;
}

function clearModels(models) {
  // TODO: Iterate over each object rather than hardcoding it
  models.Endpoints.SleepSummary.model.remove({}, function(err) {
     console.log('Cleaning SleepSummary Model');
  });

  models.LastUpdateTime.remove({}, function(err) {
     console.log('Cleaning LastUpdateTime Model');
  });
}

function generateSampleData(models) {
  // TODO: For now set up some test data
  //       This should eventually be in a separate json test file
  var samples = [];

  samples.push(new models.Endpoints.SleepSummary.model({
    humanId: '5dc2527186aaf9de560e5841f1a44bd6',
    id: '54e6d1da4159b23f4d8f6eff',
    userId: '52e20cb2fff56aac62000001',
    date: '2015-02-19',
    source: 'jawbone',
    timeAsleep: 454.65,
    timeAwake: 14,
    createdAt: '2015-02-20T06:19:06.071Z',
    updatedAt: '2015-02-20T23:01:37.053Z'
  }));

  samples.push(new models.Endpoints.SleepSummary.model({
    humanId: "5dc2527186aaf9de560e5841f1a44bd6",
    id: "54e689eadc02c44e608b8840",
    userId: "52e20cb2fff56aac62000001",
    date: "2015-02-19",
    source: "fitbit",
    timeAsleep: 434,
    timeAwake: 21,
    createdAt: "2015-02-20T01:12:10.688Z",
    updatedAt: "2015-02-20T01:12:10.688Z"
  }));

  samples.push(new models.Endpoints.SleepSummary.model({
    humanId: "5dc2527186aaf9de560e5841f1a44bd6",
    id: "54e588267b502e3e2ccc9943",
    userId: "52e20cb2fff56aac62000001",
    date: "2015-02-18",
    source: "jawbone",
    timeAsleep: 544.1333333333333,
    timeAwake: 26,
    createdAt: "2015-02-19T06:52:22.705Z",
    updatedAt: "2015-02-20T23:01:37.067Z"
  }));

  samples.push(new models.Endpoints.SleepSummary.model({
    humanId: "5dc2527186aaf9de560e5841f1a44bd6",
    id: "54e538130a65a7d83343bee9",
    userId: "52e20cb2fff56aac62000001",
    date: "2015-02-17",
    source: "fitbit",
    timeAsleep: 406,
    timeAwake: 18,
    createdAt: "2015-02-19T01:10:43.813Z",
    updatedAt: "2015-02-19T01:10:43.813Z"
  }));

  samples.push(new models.LastUpdateTime({
    humanId: '5dc2527186aaf9de560e5841f1a44bd6',
    accessToken: 'demo',
    lastUpdateTime: '2015-02-20',
    endpoint: 'sleepsummaries'
  }));

  dbutils.saveAll(samples);
}

module.exports = function(mongoose) {

  var schemas = generateSchemas(mongoose);
  var models = generateModels(mongoose, schemas);

  generateSampleData(models);

  return models;
}