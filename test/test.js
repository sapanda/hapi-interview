var rewire = require('rewire');
var expect = require('chai').expect;
var dbutils = require('../app/dbutils.js');
var supertest = require('supertest');
var tasks = rewire('../app/tasks.js');
var request = supertest(tasks);

describe('Basic Test to check whether data updates correctly in place', function() {
  this.timeout(0);
  var models = tasks.__get__('models');

  // TODO: Load sample data from a test JSON file
  var originalSleepSummary = {
    humanId: '5dc2527186aaf9de560e5841f1a44bd6',
    id: '54e6d1da4159b23f4d8f6eff',
    userId: '52e20cb2fff56aac62000001',
    date: '2015-02-19',
    source: 'jawbone',
    timeAsleep: 454,
    timeAwake: 14,
    createdAt: '2015-02-20T06:19:06.071Z',
    updatedAt: '2015-02-20T23:01:37.053Z'
  };

  var originalLastUpdateTime = {
    humanId: '5dc2527186aaf9de560e5841f1a44bd6',
    accessToken: 'demo',
    lastUpdateTime: '2015-02-20T23:01:37.053Z',
    endpoint: 'sleepsummaries'
  };

  var newSleepSummary = [{
    humanId: '5dc2527186aaf9de560e5841f1a44bd6',
    id: '54e6d1da4159b23f4d8f6eff',
    userId: '52e20cb2fff56aac62000001',
    date: '2015-02-19',
    source: 'fitbit',
    timeAsleep: 500,
    timeAwake: 15,
    createdAt: '2015-02-20T06:19:06.071Z',
    updatedAt: '2015-02-20T23:02:37.053Z'
  },
  {
    humanId: '5dc2527186aaf9de560e5841f1a44bd6',
    id: '54e6d1da4159b23f4d8f6eff',
    userId: '52e20cb2fff56aac62000001',
    date: '2015-02-19',
    source: 'jawbone',
    timeAsleep: 454.65,
    timeAwake: 14,
    createdAt: '2015-02-20T06:19:06.071Z',
    updatedAt: '2015-02-21T23:02:37.053Z'
  }];

  var newLastUpdateTime = {
    humanId: '5dc2527186aaf9de560e5841f1a44bd6',
    accessToken: 'demo',
    lastUpdateTime: '2015-02-21T23:02:37.053Z',
    endpoint: 'sleepsummaries'
  };

  function clearDB() {
    models.LastUpdateTime.remove({}, function(err) {
      models.Endpoints.SleepSummary.model.remove({}, function(err) {});
    });
  }

  function saveOriginalData() {
    var samples = [];

    samples.push(new models.Endpoints.SleepSummary.model(originalSleepSummary));
    samples.push(new models.LastUpdateTime(originalLastUpdateTime));

    dbutils.saveAll(samples);
  }

  function updateDB() {
    var newSleepSummaryClone = JSON.parse(JSON.stringify(newSleepSummary));
    tasks.__get__('updateDB')(newSleepSummaryClone, models.Endpoints.SleepSummary.model);
  }

  function verifyDB(done, size) {
    models.LastUpdateTime.find(function(err, data) {
      expect(data.length).to.equal(1);
      expect(data[0]['humanId']).to.equal(newLastUpdateTime['humanId']);
      expect(data[0]['lastUpdateTime']).to.equal(newLastUpdateTime['lastUpdateTime']);

      models.Endpoints.SleepSummary.model.find(function(err, data) {
        expect(data.length).to.equal(size);
        // TODO: Actually verify the contents of the returned data
        done();
      });
    });
  }

  function runTest(newData, recordLength, done) {
    clearDB();
    // Using timeouts since the tested functions currently don't have callbacks
    setTimeout(function() {
      saveOriginalData();
    }, 1000);
    setTimeout(function() {
      updateDB(newData);
    }, 2000);
    setTimeout(function() {
      verifyDB(done, recordLength);
    }, 3000);
  }

  it('checks to see if one of the two records gets updated in place', function(done) {
    runTest(newSleepSummary, 2, done);
  });

  it('checks to see if both records are inserted into the DB', function(done) {
    newSleepSummary[1]['date'] = '2015-02-18';
    runTest(newSleepSummary, 3, done);
  });
});