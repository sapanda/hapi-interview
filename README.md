# Human API Data Sync Server

This is a basic implementation of a server that synchronizes data stored locally across that contained on Human API. Every 30 minutes, (humanId, lastUpdateTime, datatype) tuples are retrieved from the DB. For each of these tuples, the latest data is requested in serial via the Human API Data API. Once retrieved, the data is normalized and added to DB-update queue.

## Running the server

`> npm start`

## Testing

Run all test cases using the following command:

`> npm test`
